<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //Login
    public function login(Request $req)
    {
        $credentials = request(["email", "password"]);
        if (!Auth::attempt($credentials)) {
            $err = "Não autorizado!";
            $res = [
                "error" => $err,
            ];
            return response()->json($res, 401);
        }

        $user = $req->user();

        $res["name"] = $user->name;
        $res["email"] = $user->email;
        $res['token'] = $user->createToken("token")->accessToken;
        
        $user = $res;
        
        return response()->json(compact("user"), 200);
    }

    //Logout
    public function logout(Request $req)
    {
        $isUser = $req->user()->token()->revoke();
        if ($isUser) {
            $msg = "Logout efetuado com sucesso!";
            $res = [
                "message" => $msg,
            ];

            return response()->json($res, 200);
        } else {
            $err = "Algo deu errado!";
            $res = [
                "error" => $err,
            ];

            return response()->json($res, 401);
        }
    }
}
