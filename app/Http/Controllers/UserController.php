<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(Request $req){
        $users = User::where("id", "!=", Auth::user()->id)->get();
        
        return response()->json(compact("users"), 200);
    }

    public function show(Request $req){
        $user = Auth::user();

        return response()->json(compact("user"),200);

    }


    public function update(Request $req){
        $id = Auth::user()->id;

        $this->validate($req, [
            "name" => [
                "required",
                "string",
            ],
            "email" => [
                "required",
                "email",
                "unique:users,email,{$id},id"
            ],
            "password" => [
                "nullable",
            ]
        ]);

        $data = $req->only("name", "email");
        if ($req->password) {
            $data['password'] = bcrypt($req->password);
        }
        $model = new User;

        $user = $model->findOrFail($id);
        $user->update($data);
        return response()->json(compact("user"), 200);
    }

    public function destroy($id){
        $model = new User;
        $user = $model->findOrFail($id)->delete();
        return response()->json(compact("user"), 200);
    }
}
