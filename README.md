# Backend-Laravel-Project

Feito com carinho por @vitorchumbinho123 :heart:

## Passo a passo  

   1. [Clonar o repositorio](#clonar-o-repositorio)
   2. [Executar o laravel sail](#executar-o-laravel-sail)
   3. [Tudo Pronto](#tudo-pronto)
   
<br>

## Clonar o repositorio
Para clonar o repositorio, copie e cole o seguinte comando em seu termina.

```
$ git clone https://gitlab.com/vitorchumbinho123/backend-laravel-project.git
```

<br>

## Executar o laravel sail

__É necessário ter o docker-composer instalado.__ https://docs.docker.com/compose/install/

Abra o terminal dentro da pasta do projeto, cole o codigo abaixo e aguarde.

```
$ ./vendor/bin/sail up -d
```

Caso queira parar a aplicação, copie e cole o comando abaixo no terminal dentro da pasta do projeto.

```
$ ./vendor/bin/sail down
```
<br>

## Tudo pronto

Agora a api já está funcionando.

Essa api funcionara em conjunto com o projeto que esta neste repositorio https://gitlab.com/vitorchumbinho123/frontend-angular-project. 
