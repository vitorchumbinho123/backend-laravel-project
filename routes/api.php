<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\SignUpController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["middleware" => ["guest:api"]], function () {
    Route::post("/users/login", [LoginController::class, "login"]);
    Route::post("/users/sign-up", [SignUpController::class, "signUp"]);


});

Route::group(["middleware" => ["auth:api"]], function () {
    Route::get("/users/show", [UserController::class, "show"]);
    Route::get("/users", [UserController::class, "index"]);
    
    Route::post("/users/logout", [LoginController::class, "logout"]);

    Route::put("/users/update", [UserController::class, "update"]);

    Route::delete("/users/delete/{id}", [UserController::class, "destroy"]);
});