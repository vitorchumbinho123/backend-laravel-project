<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SignUpController extends Controller
{
    public function signUp(Request $req)
    {
        $this->validate($req,[
        "name" => [
            "required",
            "string",
        ],
        "email" => [
            "required",
            "email",
            "unique:users"
        ],
        "password" => [
            "required",
        ]]);

        $user = User::create([
            "name" => $req->name,
            "email" => $req->email,
            "password" => bcrypt($req->password),
        ]);

        return response(["message" => "Usuario registrado com sucesso!"], 200);
    }
}
